const Ajv = require('ajv');
const Garden = require('../models/Garden');
const logger = require('../logger')

const gardenSchema = {
  type: 'object',
  properties: {
    name: { type: 'string' },
    description: {type: 'string' },
  },
}

const ajv = new Ajv();

/* GET users listing. */
exports.listAll = (req, res, _next) => {
  logger.info('getting gardens')
  Garden.findAll()
    .then((gardens) => {
      return res.json(gardens);
    })
};

/* GET users listing. */
exports.listForUser = (req, res, _next) => {
  logger.info('getting my gardens')
  if (req.isAuthenticated()) {
    logger.info('getting my gardens')
    const owner = req.user.externalId;
    return Gardens.findAll({
      where: {
        owner: owner
      }
    })
      .then((gardens) => {
        return res.json(gardens);
      });
  } else {
    logger.info('Not authed!')
    return res.status(403).json({message: 'Please Authenticate'})
  }
};

exports.getGarden = (req, res, _next) => {
  logger.info('getting garden');
  return Garden.findOne({
    where: { id : req.params.gardenID }
  }).then(garden => {
    return res.json({
      garden
    });
  }).catch(error => {
    return res.status(500).json(error);
  });
}


exports.update = (req, res, _next) => {
  logger.info('updating garden');
  const updatedGarden = req.body;
  if (!validate(garden)) {
    return res.status(401).json({'message':'invalid arguments'});
  }
  updatedGarden.externalId = req.params;
  return Garden.findOne({
    where: {
      externalId: garden.externalId
    }
  }).tap((retrievedGarden) => {
    if (!retrievedGarden)
      Promise.reject({error: 'not found'});
  }).then(retrievedGarden => {
    // should check for owner or god permission...


    retrievedGarden.update(updatedGarden)
  }).then(savedGarden => {
    logger.info({ message: 'Garden saved', garden: savedGarden});
    return res.json(savedGarden);
  }).catch((err) => {
    return res.status(401).json({message: 'error updating garden', err})
  })
}

exports.create = (req, res, _next) => {
  logger.info('Creating new garden');
  const newGarden = req.body;
  const validate = ajv.compile(gardenSchema);
  if (!validate(newGarden)) {
    return res.status(401).json({'message':'invalid argument'});
  }
  newGarden.owner = req.user.externalId;
  logger.info(newGarden);
  return Garden.create({
    ...newGarden
  }).then(createdGarden => {
    logger.info({message: 'Garden created'});
    return res.json(createdGarden);
  })
  .catch((err) => {
    logger.info(err)
    return res.status(401).json(err);
  });
}

exports.delete = (req, res, _next) => {
  const {
    externalId,
  } = req.params;
  return Garden.findOne({
    where: {
      externalId: externalId,
    },
  })
  .then(garden =>
    // should check for owner or god permission...
    garden.destroy())
  .then(() => res.json({success}))
  .catch((err) => { throw err; });
}