const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');

const {
  User,
  Garden,
} = require('../models');

const {
  expect,
} = chai;
const app = require('../app');
const Garden = require('../models/Garden');
const { list } = require('../../users/controllers/UserController');

chai.use(chaiHttp);

before(() => {
});
  
after(() => {
});

describe('Garden checks', () => {
    let listStub;
    let findStub;
    let userStub;
  
    const gardensData = [
        {
            id: 0,
            externalId: 'G1234',
            name: 'graden1',
            description: 'first garden',
            owner: '1234'
        },
        {
            id: 1,
            externalId: 'G5678',
            name: 'graden2',
            description: 'second garden',
            owner: '1234'
        },
    ];

    before(() => {
      listStub = sinon.stub(Garden, 'findAll')
      listStub.returns(Promise.resolve(gardensData));
      const ownerQuery = {
        where: {
          owner: '1234'
        }
      }
      listStub.withArgs(ownerQuery).returns(Promise.resolve(gardensData));
      const ownerQuery2 = {
        where: {
          owner: '5678'
        }
      }
      listStub.withArgs(ownerQuery2).returns(Promise.resolve([]));
      



      
    });
})