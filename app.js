const createError = require('http-errors');
const express = require('express');
const uuid = require('uuid').v4;
const cookieParser = require('cookie-parser');

const gardenRouter = require('./routes/garden');

require('./passportStrategies');
const passport = require('passport');

const DSNParser = require('dsn-parser');
const sessionPool = require('pg').Pool;
const session = require('express-session');
const logger = require('./logger');
const pgSession = require('connect-pg-simple')(session);
const winston = require('winston');
const expressWinston = require("express-winston");

const app = express();

app.use(expressWinston.logger({
  transports: [
       new (winston.transports.Console)({
          handleExceptions: true,
          timestamp: true,
          level:"info"
      })
  ],
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute: function (req, res) { return false; } // optional: allows to skip some log messages based on request and/or response
}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


const dsn = new DSNParser(process.env.DB_CONNECTION);
const dsnConfig = dsn.getParts();

const DBPassword = process.env.DB_PASSWORD;
const password = DBPassword || dsnConfig.password;

const sessionDBaccess = new sessionPool({
    user: dsnConfig.user,
    password: password,
    host: dsnConfig.host,
    port: dsnConfig.port,
    database: dsnConfig.database})

app.use(session({
genid: (req) => {
    logger.info("Generating uuid...")
    return uuid();
},
store: new pgSession({
    pool: sessionDBaccess,
    tableName: 'session'
}),
secret: 'dingo',
resave: false,
saveUninitialized: true
}))

app.use(passport.initialize());
app.use(passport.session());


app.use('/', gardenRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500).json({'message':'garden server error'});
});

module.exports = app;
