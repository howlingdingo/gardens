const winston = require('winston');


const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'garden-service' },
  transports: [
    new winston.transports.Console({ level: 'silly' }),
  ],
});

module.exports = logger;
