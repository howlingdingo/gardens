/* eslint no-param-reassign:
  ["error", { "props": true, "ignorePropertyModificationsFor": ["user"] }] */
  const Sequelize = require('sequelize');
  const DSNParser = require('dsn-parser');
  const logger = require('../logger');

  // Connect to the database.
  const db = {};
  const DbConnectionString = process.env.DB_CONNECTION;
  const DbPassword = process.env.DB_PASSWORD;

  const dsn = new DSNParser(DbConnectionString);
  const dsnConfig = dsn.getParts();
  
  const password = DbPassword || dsnConfig.password;

  const sequelize = new Sequelize(dsnConfig.database, dsnConfig.user, password, {
    host: dsnConfig.host,
    port: dsnConfig.port,
    // log all SQL requests at the "silly" level.
    logging: (mesg) => logger.silly(mesg),
    pool: {
      max: 2,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    dialect: 'postgres',
  });

  db.sequelize = sequelize;
  // Connect to the database
  db.sequelize
    .authenticate()
    .then(() => {
      logger.info({ message: 'Connected to database' });
    })
    .catch((err) => {
      logger.error({ message: 'Unable to connect to database' });
      logger.error(err);
      process.exit(1);
    });
  
  const { DataTypes } = Sequelize;
  
  const User = sequelize.define(
    'User',
    {
      externalId: {
        type: DataTypes.UUID,
        field: 'external_id',
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      name: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: 'User name must be provided.',
          },
        },
      },
    },
    {
      tableName: 'users',
      timestamps: true,
      paranoid: true,
      underscored: true,
      hooks: {
        beforeSave: (user) => {
          throw({message: 'no user save from this service'});
        },
      },
    },
  );
  
  User.prototype.getFields = function userFields() {
    return ['uniqueId', 'name' ];
  };
  
  User.prototype.toJSON = function userToJson() {
    const values = { ...this.get() };
    return values;
  };
  
  module.exports = User;
  