const express = require('express');
const router = express.Router();
const gardenController = require('../controllers/gardenController')

const checkAuth = (controller) => {
  return (req, res, next) => {
    if (req.isAuthenticated()) {
      return controller(req, res, next);
    }
    logger.info('Not authed!')
    return res.status(403).json({message: 'Please Authenticate'})
  };
};

/* List gardens */
router.get('/gardens', gardenController.listAll);
router.get('/mygardens', checkAuth(gardenController.listForUser));

/* Individual gardens */
router.get('/:gardenId', checkAuth(gardenController.getGarden))
router.patch('/:gardenId', checkAuth(gardenController.update));
router.post('/gardens', checkAuth(gardenController.create));
router.delete('/:gardenId', checkAuth(gardenController.delete));


module.exports = router;
