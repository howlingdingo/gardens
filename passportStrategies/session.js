/**
 * Module dependencies
 */
 const passport = require('passport');

 const logger = require('../logger');
 const User = require('../models/User');
 logger.info('--> loading strategy - session')

passport.serializeUser((user, done) => {
    logger.info(`serialising user - id: ${user.id}`);
    return done(null, user.id)
  })
  
  passport.deserializeUser((id, done) => {
    logger.info(`deserialising ...`);
    
    logger.info(`deserialising user - id: ${id}`);
    User.findOne({
      where: {
        id: id,
      },
    })
    .then(user => { 
      logger.info(`have user ds`) 
      logger.info(`user: ${user}`)
      done(null, user) 
    })
    .catch(error => {
      logger.info('Error?');
      done(error, false)
    });
  })
  